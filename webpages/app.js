const express = require('express');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.render('home');
});

app.get('/objetivo', (req, res) => {
  res.render('objetivo');
});

app.get('/desenvolvimento', (req, res) => {
  res.render('desenvolvimento');
});

app.get('/conclusao', (req, res) => {
  res.render('conclusao');
});


// Rotas
const homeRoute = require('./routes/home');
const objetivoRoute = require('./routes/objetivo');
const desenvolvimentoRoute = require('./routes/desenvolvimento');
const conclusaoRoute = require('./routes/conclusao');

app.use('/', homeRoute);
app.use('/objetivo', objetivoRoute);
app.use('/desenvolvimento', desenvolvimentoRoute);
app.use('/conclusao', conclusaoRoute);

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
